from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.forms import ModelForm, CheckboxSelectMultiple, BaseModelFormSet, Form
from django import forms
from .models import Profile, Company, Premises, Days


class UserRegistration(UserCreationForm):
    """
    Form for registering new users
    """

    class Meta:
        model = User
        fields = (
            "username",
            "password1",
            "password2",
            "email",
            "first_name",
            "last_name",
        )


class SimpleLoginForm(AuthenticationForm):
    """
    SimpleLoginForm is equal to default AuthenticationForm
    """


class CompanyRegistration(ModelForm):
    """
    Form for creating new company under user
    """

    class Meta:
        model = Company
        fields = [
            "name",
            "vat",
            "company_id",
            "address1",
            "address2",
            "zip_code",
            "city",
            "country",
        ]


class PremisesRegistration(ModelForm):
    """
    Form for creating Premises
    """

    def __init__(self, *args, **kwargs):
        current_user = kwargs.get("user")
        super().__init__(*args, **kwargs)
        self.fields["company"].queryset = Company.objects.filter(owner=current_user)

    class Meta:
        model = Premises
        fields = "__all__"
        widgets = {"working_days": CheckboxSelectMultiple}
