from django.apps import AppConfig


class CoreConfig(AppConfig):
    name ='app.Django_Reservation_System.core'
