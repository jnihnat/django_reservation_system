import random, datetime
from django.shortcuts import redirect, HttpResponse, reverse
from django.contrib.auth import login
from .forms import (
    UserRegistration,
    SimpleLoginForm,
    CompanyRegistration,
    PremisesRegistration,
)
from django.views.generic.edit import CreateView
from django.views.generic import DetailView, TemplateView
from django.core.mail import EmailMessage
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import (
    urlsafe_base64_encode,
    urlsafe_base64_decode,
    url_has_allowed_host_and_scheme,
)
from django.template.loader import render_to_string
from .token import account_activation_token
from .models import Profile, Worker, WorkingTime
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User


class RegisterUserForm(CreateView):
    form_class = UserRegistration
    template_name = "core/RegisterUser.html"
    success_url = "core:home"

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        self.object = None
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            person = Profile.objects.create(user=user)
            person.save()
            current_site = get_current_site(request)
            mail_subject = "Activate your blog account."
            message = render_to_string(
                "acc_active_email.html",
                {
                    "user": user,
                    "domain": current_site.domain,
                    "uid": urlsafe_base64_encode(force_bytes(user.pk)),
                    "token": account_activation_token.make_token(user),
                },
            )
            to_email = form.cleaned_data.get("email")
            email = EmailMessage(mail_subject, message, to=[to_email])
            email.send()
            return HttpResponseRedirect(reverse("core:home"))
        else:
            return self.form_invalid(form)


def activate(request, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64)
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
        return HttpResponse("User None!")
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        return redirect("core:profile", user.username)
    else:
        return HttpResponse("Activation lk s invalid!")


class ProfileView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    login_url = "core:login"
    redirect_field_name = "redirect_to"
    model = User
    slug_field = "username"
    template_name = "core/profile.html"

    def test_func(self):
        return self.request.user.username == self.kwargs["slug"]


class SimpleLoginPage(LoginView):
    template_name = "core/primitive-login-page.html"
    authentication_form = SimpleLoginForm
    redirect_authenticated_user = False

    def get_redirect_url(self):
        try:
            username = self.request.user.username
            redirect_to = "/dashboard/" + username  # upravit
            return redirect_to
        finally:
            super().get_redirect_url()


class HomeView(TemplateView):
    template_name = "core/home.html"


class SimpleLogoutView(LogoutView):
    next_page = "core:home"


class RegisterCompanyView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    login_url = "core:login"
    slug_field = "username"
    template_name = "core/RegisterCompany.html"
    form_class = CompanyRegistration

    def test_func(self):
        return self.request.user.username == self.kwargs["slug"]

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        self.object = None
        if form.is_valid():
            company = form.save(commit=False)
            company.save()
            profile = request.user.profile
            company.owner.add(profile)
            return HttpResponseRedirect(
                reverse("core:profile", args=[request.user.username])
            )
        else:
            return self.form_invalid(form)


class RegisterPremisesView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    login_url = "core:login"
    slug_field = "username"
    template_name = "core/RegisterPremises.html"
    form_class = PremisesRegistration

    def test_func(self):
        return self.request.user.username == self.kwargs["slug"]

    def handle_no_permission(self):
        return redirect("core:login")

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        self.object = None
        if form.is_valid():
            premises = form.save(commit=False)
            premises.save()
            for day in form.cleaned_data.get("working_days"):
                premises.working_days.add(day)
            first_name_list = request.POST.getlist("first_name")
            last_name_list = request.POST.getlist("last_name")
            for x, first_name in enumerate(first_name_list):
                last_name = last_name_list[x]
                random_username = (
                    first_name + last_name + str(random.randint(1, 100000000))
                )
                user = User.objects.create_user(
                    first_name=first_name, last_name=last_name, username=random_username
                )
                user.is_active = False
                user.save()
                person = Profile.objects.create(user=user)
                person.save()
                worker = Worker.objects.create(profile=person)
                worker.save()
                premises.worker.add(worker)
                times = WorkingTime.objects.create(
                    premise=premises,
                    worker=None,
                    valid_from=datetime.date.today(),
                    valid_to=datetime.date(2999, 12, 1),
                    opening_hours=request.POST.get("opening_time"),
                    closing_hours=request.POST.get("closing_time"),
                )
            return HttpResponseRedirect(
                reverse("core:profile", args=[request.user.username])
            )
        else:
            return self.form_invalid(form)
