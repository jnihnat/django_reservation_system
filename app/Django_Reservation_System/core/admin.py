from django.contrib import admin
from .models import Profile, Company, Days, Premises, Services, DefaultServices
# for time being just for basics, will be upgraded later in project


class CompanyInline(admin.TabularInline):
    model = Company.owner.through
    fields = ["comp_name", "vat_number"]
    readonly_fields = ["comp_name", "vat_number"]

    def comp_name(self, instance):
        return instance.company.name

    comp_name.short_description = "Company name"

    def vat_number(self, instance):
        return instance.company.vat

    vat_number.short_description = "VAT number"


class ProfileAdmin(admin.ModelAdmin):
    fieldsets = [
        ("UserInfo", {"fields": ["user", "person_type",]}),
    ]
    inlines = [CompanyInline]


admin.site.register(Profile, ProfileAdmin)
admin.site.register(Company)
admin.site.register(Days)
admin.site.register(Premises)
admin.site.register(DefaultServices)
admin.site.register(Services)
