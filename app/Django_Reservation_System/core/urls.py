from django.urls import path

from . import views

app_name = "core"
urlpatterns = [
    path("register", views.RegisterUserForm.as_view(), name="register_admin"),
    path("dashboard/<slug>", views.ProfileView.as_view(), name="profile"),
    path("", views.HomeView.as_view(), name="home"),
    path("activate/<uidb64>/<token>/", views.activate, name="activation", ),
    path("login/", views.SimpleLoginPage.as_view(), name="login"),
    path("logout/", views.SimpleLogoutView.as_view(), name="logout"),
    path(
        "dashboard/<slug>/companyregistration",
        views.RegisterCompanyView.as_view(),
        name="register_company",
    ),
    path(
        "dashboard/<slug>/premisesregistration",
        views.RegisterPremisesView.as_view(),
        name="register_premises",
    ),
]
