"""
Core models
"""
from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse


class Days(models.Model):
    day = models.CharField(max_length=9)

    def __str__(self):
        return self.day


class Profile(models.Model):
    """
    Model for company admins of reservation system
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        permissions = [
            ("create_company", "Can create company"),
            ("edit_company", "Can edit company"),
            ("remove_company", "Can remove company"),
        ]

    def get_absolute_url(self):
        return reverse("products:product", kwargs={"slug": self.user.username})

    def __str__(self):
        return "%s %s" % (self.user.first_name, self.user.last_name)


class Worker(models.Model):
    """
    Model for worker
    """

    profile = models.OneToOneField(Profile, on_delete=models.CASCADE)

    def __str__(self):
        return "%s %s" % (self.profile.user.first_name, self.profile.user.last_name)


class Company(models.Model):
    """
    Model for company informations
    """

    id = models.AutoField(primary_key=True)
    name = models.CharField("Company name", max_length=200)
    company_id = models.IntegerField("Company registration number")
    vat = models.CharField("Vat number", max_length=15)
    address1 = models.CharField("Address line 1", max_length=1024,)
    address2 = models.CharField("Address line 2", max_length=1024,)
    zip_code = models.CharField("ZIP / Postal code", max_length=6,)
    city = models.CharField("City", max_length=1024,)
    country = models.CharField("Country", max_length=30,)
    owner = models.ManyToManyField(Profile)

    def __str__(self):
        return "%s" % self.name


class Premises(models.Model):
    """
    Model for companies premises
    """

    id = models.AutoField(primary_key=True)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    name = models.CharField("Premises name", max_length=200)
    site_name = models.CharField("Site name", max_length=20, unique=True)
    working_days = models.ManyToManyField(Days)
    worker = models.ManyToManyField(Worker, blank=True)
    premises_type = models.CharField("Premises type", max_length=50)

    def __str__(self):
        return "%s" % self.name


class Holydays(models.Model):
    """
    Model for Workers holydays
    """

    worker = models.ForeignKey(Worker, on_delete=models.CASCADE)
    date_from = models.DateField("Date From")
    date_to = models.DateField("Date To")
    time_from = models.TimeField("Time From", null=True, blank=True)
    time_to = models.TimeField("Time To", null=True, blank=True)


class WorkingTime(models.Model):
    """
    Default working days and hours for Premises and Workers
    """

    premise = models.ForeignKey(Premises, on_delete=models.CASCADE, blank=True)
    worker = models.ForeignKey(Worker, on_delete=models.CASCADE, blank=True, null=True)
    valid_from = models.DateField("Valid From")
    valid_to = models.DateField("Valid To")
    opening_hours = models.TimeField()
    closing_hours = models.TimeField()


class DefaultServices(models.Model):
    name = models.CharField("Service name", max_length=30)
    description = models.TextField("Service description")
    price = models.FloatField("Service price")


class Services(DefaultServices, models.Model):
    """
    Services for every premises
    """

    premises = models.ForeignKey(Premises, on_delete=models.CASCADE)
    worker = models.ManyToManyField(Worker, blank=True)
