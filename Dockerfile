FROM python:3.8-slim
ARG REQUIREMENTS_FILE=requirements

LABEL maintainer jnihnat@gmail.com

COPY app /var/www/app
WORKDIR /var/www/app

RUN pip install -r ${REQUIREMENTS_FILE}

ENTRYPOINT ./startup.sh
