How to run the app
==================
App is configured, so it can be run as Django local app and as docker image.

Django local
""""""""""""
Is run by default python interpreter using manage.py.
Steps provided to run:

* set default environment variable "DJANGO_ENV" to "LOCAL".
  can be set in pycharm run configuration or in OS
* run "python manage.py migrate"
* run "python manage.py collectstatic"
* run "python manage.py runserver"

you can now access web site on url `<http://127.0.0.1:8000>`_

* to create admin user run "python manage.py createsuperuser"
* you can access admin site on `<http://127.0.0.1:8000/admin>`_

Django Docker
"""""""""""""

* You have to have installed docker and than run docker machine.
* Then find out docker machine ip adress with command docker-machine ip
* Then add this url to your dns hosts list
    * for windows go to c:\Windows\System32\drivers\etc\
       edit hosts file - add ip from above and url reservationsystem.test
    * for mac and linux open terminal and run sudo nano /etc/hosts
        add ip from above and url reservationsystem.test
* Now connect to your docker machine with terminal command
    docker-machine ssh default
* Edit hosts as described above, but use 127.0.0.1 as ip
* Exit docker-machine
* run docker-compose in terminal:
    docker-compose -f docker-compose.dev.yaml up -d --build
* you can access site on url `<https://reservationsystem.test:8000/>`_

Django Admin site
""""""""""""""""""
For dev environment, there is set  admin user at start up with credentials:

* username: admin
* password: DevPassword





