#! /bin/sh

from django.contrib.auth.models import Group, Permission

g1 = Group.objects.create(name='CompanyAdmin')
p1 = Permission.objects.get_or_create(name='permissions')
g1.permissions.add(p1)
