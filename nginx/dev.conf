server {

    if ($host = reservationsystem.test) {
        return 301 https://$host$request_uri;
    } # managed by Certbot

    listen 80;
    server_name reservationsystem.test;
    return 404; # managed by Certbot
}

server {
    listen 443 ssl http2; # managed by Certbot
    server_name reservationsystem.test;
    access_log /var/log/nginx/reservation_system.log;

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_certificate   /etc/nginx/certs/cert.crt;
    ssl_certificate_key /etc/nginx/certs/cert.key;

    proxy_read_timeout 300;
    keepalive_timeout 300;
    client_max_body_size 30M;

    location /static/ {
        alias /static/;
    }

    location /media/ {
        alias /var/www/app/Django_Reservation_System/media/;
    }

    location ~* favicon|android-chrome|safari-pinned-tab|apple-touch-icon|ms-icon|browserconfig.xml|manifest {
        root /var/www/app/Django_Reservation_System/static/images/favicon;
    }

    location / {
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_redirect off;

        if (!-f $request_filename) {
            proxy_pass http://reservation_system:8000;
            break;
        }
    }
}
